from fastapi import FastAPI,Path
import requests
from pydantic import BaseModel
from utils import scrap_web_page,scrap_web_page_element

app = FastAPI()

key = {
    1:{
        "api_key":"f70459ff-0ec5-4cda-8a3e-d6de428c625a"
    },
    2:{
        "api_key":"2fabb9c4-cc78-4266-a5ec-3d1927afac4f"
    },
    3:{
        "api_key":"08e137b0-532b-4094-8aaf-ee4e0ee343e9"
    },
}

class Url(BaseModel):
    scrap_url:str

@app.get("/")

def index():
    return {"name":"Scrap Api"}

@app.post("/scrap/list/{api}")

def scrap(api_key:str, url:Url):
    for i in key:
        if api_key != key[i]["api_key"]:
            return {"error":"Api key doesn't match"}
        else:
            scrapped_data = scrap_web_page(url.scrap_url)
            if scrapped_data:
                return {"api_key":api_key, "data":scrapped_data}
            else:
                return {"error":"Wrong web page to scrap"}

@app.post("/scrap/product/{api}")

def scrap(api_key:str, url:Url):
    for i in key:
        if api_key != key[i]["api_key"]:
            return {"error":"Api key doesn't match"}
        else:
            scrapped_data = scrap_web_page_element(url.scrap_url)
            if scrapped_data:
                return {"api_key":api_key, "data":scrapped_data}
            else:
                return {"error":"Wrong web page to scrap"}