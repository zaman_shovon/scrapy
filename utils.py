import requests
import uuid
import json
from bs4 import BeautifulSoup
from urllib import parse

def get_url_paramter(url):
    parsed_url = parse.parse_qs(parse.urlsplit(url).query)
    if 'p' in parsed_url:
        return parsed_url['p'][0]
    else:
        return False

def get_current_path(url):
    parsed_url = parse.urlparse(url).path
    return parsed_url.split('/')[1].split('.html')[0]

def scrap_web_page(url):
    data = []
    URL = url
    try:
        # Parsing Url
        current_page = get_url_paramter(URL)
        current_path = get_current_path(URL)
        # Extract the data from site
        page = requests.get(URL)
        soup = BeautifulSoup(page.content,"html.parser")
        product_section = soup.find_all("li",class_="item product product-item")
        # Find the product element and parse them as object
        for product_details in product_section:
            p_name = ''
            p_price = ''
            p_image = ''
            p_discount = ''
            p_page = 0
            for product_name in product_details.find_all("a",class_="product-item-link"):
                p_name = product_name.text.strip()
            for product_price in product_details.find_all("span",class_="price"):
                p_price = product_price.text.strip()
            for product_image in product_details.find_all("img",class_="product-image-photo"):
                p_image = product_image["src"]
            for product_discount in product_details.find_all("span",class_="discount-percent"):
                p_discount = product_discount.text.strip()
            list = {
                'uuid':str(uuid.uuid4()),
                'name':p_name,
                'price':str(p_price),
                'image':p_image,
                'discount':p_discount if p_discount and p_discount!='' else 'No Discount Available',
                'current_page':current_page if current_page else 1
            }
            data.append(list)
        return data
    except Exception as ex:
        return False
    # # Create json file to save
    # if len(data) > 0:
    #     file_name = current_path+"_page "+str(current_page) if current_page else str(0)
    #     file = file_name+".json"
    #     with open(file,"w") as json_file:
    #         json_file.write(json.dumps(data,indent = 1))

def scrap_web_page_element(url):
    data = []
    URL = url
    try:
        # Parsing Url
        current_page = get_url_paramter(URL)
        current_path = get_current_path(URL)
        # Extract the data from site by installing selenium
        # page = driver.get(URL) # for loading original js generated elements
        # html = page.page_source # for loading original js generated elements
        page = requests.get(URL)
        soup = BeautifulSoup(page.content,"html.parser")
        product_section = soup.find("div",class_="product-info-main")
        # Find the product element and parse them as object
        p_price = ''
        p_old_price = ''
        p_name = product_section.find("h1",class_="page-title").text.strip()
        p_brand = product_section.find("div",class_="product attibute manufacturer").find("span").text.strip()
        p_vendor = product_section.find("div",class_="vendor-info").find("a").text.strip()
        p_discount = product_section.find("span",class_="discount-percent").text.strip() if product_section.find("span",class_="discount-percent") else 'No Discount Available for this item'
        p_warranty = product_section.find("div",class_="product-warranty").find("span").text.strip()
        p_emi = product_section.find("div",class_="emi-start-price").find("strong").text.strip()
        p_review_count = soup.find("span",class_="counter").text.strip()
        p_image = ''
        p_page = 0
        for price in product_section.find_all("span",class_="price-wrapper"):
            if price["data-price-type"] == 'finalPrice':
                p_price = price.text.strip().split('৳')[1]
            p_old_price = price.text.strip().split('৳')[1] if price["data-price-type"] == 'oldPrice' else "0"

        list = {
            'uuid':str(uuid.uuid4()),
            'name':p_name,
            'price':str(p_price),
            'old price':str(p_old_price),
            'currency':'BDT',
            'discount':p_discount,
            'brand':p_brand,
            'vendor':p_vendor,
            'warranty':p_warranty,
            'EMI':p_emi,
            'reviews':p_review_count
        }
        data.append(list)
        return data
    except Exception as ex:
        return False
    # # Create json file to save
    # if len(data) > 0:
    #     file_name = current_path+"_page "+str(current_page) if current_page else str(0)
    #     file = file_name+".json"
    #     with open(file,"w") as json_file:
    #         json_file.write(json.dumps(data,indent = 1))